/*! \file  getTouch.c
 *
 *  \brief Get current touch
 *
 *
 *  \author jjmcd
 *  \date July 14, 2015, 7:51 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"


/*! getTouch() - Query the TFT for a touch response */

/*! getTouch queries the TFT for a touch event.  If the screen is
 *  being touched, the coordinates of the touch are stored in
 *  x and y.  If not, x and y contain 0xffff.  The function returns
 *  true if there was a touch event, false if not
 *
 * \param x unsigned int * - Pointer to horizontal coordinate of the touch event
 * \param y unsigned int * - Pointer to vertical coordinate of the touch event
 * \return True if touched, otherwise false
 */
int TFTgetTouch(unsigned int *x, unsigned int *y)
{
  unsigned char ch1, ch2;

  /* Request the touch event */
  putch(TFTTOUCHQUERY);

  /* Clear out the result storage */
  *x = *y = 0; /* This shouldn't be really needed */
  /* Get the two-byte X coordinate */
  ch1 = getch();
  ch2 = getch();
  /* Combine the two bytes into a 16-bit unsigned int */
  *x = ((ch2 << 8)&0xff00) | (ch1 & 0xff);
  /* Get the two-byte Y coordinate */
  ch1 = getch();
  ch2 = getch();
  /* Combine the two bytes into a 16-bit unsigned int */
  *y = ((ch2 << 8)&0xff00) | (ch1 & 0xff);
  /* In case of junk, drain the FIFO */
  clearSerial(); /* Probably overkill */
  /* TFT sends all F's if no touch data available */
  /* Returning 0 if either ffff works far better  */
  if (*x == 0xffff)
    return 0;
  if (*y == 0xffff)
    return 0;
  return 1;
}
