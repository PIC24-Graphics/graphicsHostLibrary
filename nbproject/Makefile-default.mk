#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=serialinitialize.c putch.c TFTprintDecRight.c TFTputsTT.c tputs.c delay.c TFTsend2.c TFTsend4.c idleSync.c sendWord.c TFTGcolors.c putString.c TFTGaddPoint.c graphingStrings.c clearSerial.c getch.c TFTgetTouch.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/serialinitialize.o ${OBJECTDIR}/putch.o ${OBJECTDIR}/TFTprintDecRight.o ${OBJECTDIR}/TFTputsTT.o ${OBJECTDIR}/tputs.o ${OBJECTDIR}/delay.o ${OBJECTDIR}/TFTsend2.o ${OBJECTDIR}/TFTsend4.o ${OBJECTDIR}/idleSync.o ${OBJECTDIR}/sendWord.o ${OBJECTDIR}/TFTGcolors.o ${OBJECTDIR}/putString.o ${OBJECTDIR}/TFTGaddPoint.o ${OBJECTDIR}/graphingStrings.o ${OBJECTDIR}/clearSerial.o ${OBJECTDIR}/getch.o ${OBJECTDIR}/TFTgetTouch.o
POSSIBLE_DEPFILES=${OBJECTDIR}/serialinitialize.o.d ${OBJECTDIR}/putch.o.d ${OBJECTDIR}/TFTprintDecRight.o.d ${OBJECTDIR}/TFTputsTT.o.d ${OBJECTDIR}/tputs.o.d ${OBJECTDIR}/delay.o.d ${OBJECTDIR}/TFTsend2.o.d ${OBJECTDIR}/TFTsend4.o.d ${OBJECTDIR}/idleSync.o.d ${OBJECTDIR}/sendWord.o.d ${OBJECTDIR}/TFTGcolors.o.d ${OBJECTDIR}/putString.o.d ${OBJECTDIR}/TFTGaddPoint.o.d ${OBJECTDIR}/graphingStrings.o.d ${OBJECTDIR}/clearSerial.o.d ${OBJECTDIR}/getch.o.d ${OBJECTDIR}/TFTgetTouch.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/serialinitialize.o ${OBJECTDIR}/putch.o ${OBJECTDIR}/TFTprintDecRight.o ${OBJECTDIR}/TFTputsTT.o ${OBJECTDIR}/tputs.o ${OBJECTDIR}/delay.o ${OBJECTDIR}/TFTsend2.o ${OBJECTDIR}/TFTsend4.o ${OBJECTDIR}/idleSync.o ${OBJECTDIR}/sendWord.o ${OBJECTDIR}/TFTGcolors.o ${OBJECTDIR}/putString.o ${OBJECTDIR}/TFTGaddPoint.o ${OBJECTDIR}/graphingStrings.o ${OBJECTDIR}/clearSerial.o ${OBJECTDIR}/getch.o ${OBJECTDIR}/TFTgetTouch.o

# Source Files
SOURCEFILES=serialinitialize.c putch.c TFTprintDecRight.c TFTputsTT.c tputs.c delay.c TFTsend2.c TFTsend4.c idleSync.c sendWord.c TFTGcolors.c putString.c TFTGaddPoint.c graphingStrings.c clearSerial.c getch.c TFTgetTouch.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24FV32KA301
MP_LINKER_FILE_OPTION=,--script=p24FV32KA301.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/serialinitialize.o: serialinitialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serialinitialize.o.d 
	@${RM} ${OBJECTDIR}/serialinitialize.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  serialinitialize.c  -o ${OBJECTDIR}/serialinitialize.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/serialinitialize.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/serialinitialize.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/putch.o: putch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putch.o.d 
	@${RM} ${OBJECTDIR}/putch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  putch.c  -o ${OBJECTDIR}/putch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putch.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/putch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTprintDecRight.o: TFTprintDecRight.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTprintDecRight.o.d 
	@${RM} ${OBJECTDIR}/TFTprintDecRight.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTprintDecRight.c  -o ${OBJECTDIR}/TFTprintDecRight.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTprintDecRight.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTprintDecRight.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTputsTT.o: TFTputsTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTputsTT.o.d 
	@${RM} ${OBJECTDIR}/TFTputsTT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTputsTT.c  -o ${OBJECTDIR}/TFTputsTT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTputsTT.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTputsTT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/tputs.o: tputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/tputs.o.d 
	@${RM} ${OBJECTDIR}/tputs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  tputs.c  -o ${OBJECTDIR}/tputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/tputs.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/tputs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/delay.o: delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/delay.o.d 
	@${RM} ${OBJECTDIR}/delay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  delay.c  -o ${OBJECTDIR}/delay.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/delay.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/delay.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTsend2.o: TFTsend2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTsend2.o.d 
	@${RM} ${OBJECTDIR}/TFTsend2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTsend2.c  -o ${OBJECTDIR}/TFTsend2.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTsend2.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTsend2.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTsend4.o: TFTsend4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTsend4.o.d 
	@${RM} ${OBJECTDIR}/TFTsend4.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTsend4.c  -o ${OBJECTDIR}/TFTsend4.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTsend4.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTsend4.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/idleSync.o: idleSync.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/idleSync.o.d 
	@${RM} ${OBJECTDIR}/idleSync.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  idleSync.c  -o ${OBJECTDIR}/idleSync.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/idleSync.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/idleSync.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/sendWord.o: sendWord.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sendWord.o.d 
	@${RM} ${OBJECTDIR}/sendWord.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  sendWord.c  -o ${OBJECTDIR}/sendWord.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/sendWord.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/sendWord.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTGcolors.o: TFTGcolors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGcolors.o.d 
	@${RM} ${OBJECTDIR}/TFTGcolors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTGcolors.c  -o ${OBJECTDIR}/TFTGcolors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGcolors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTGcolors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/putString.o: putString.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putString.o.d 
	@${RM} ${OBJECTDIR}/putString.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  putString.c  -o ${OBJECTDIR}/putString.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putString.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/putString.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTGaddPoint.o: TFTGaddPoint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o.d 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTGaddPoint.c  -o ${OBJECTDIR}/TFTGaddPoint.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGaddPoint.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTGaddPoint.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/graphingStrings.o: graphingStrings.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/graphingStrings.o.d 
	@${RM} ${OBJECTDIR}/graphingStrings.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  graphingStrings.c  -o ${OBJECTDIR}/graphingStrings.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/graphingStrings.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/graphingStrings.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/clearSerial.o: clearSerial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/clearSerial.o.d 
	@${RM} ${OBJECTDIR}/clearSerial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  clearSerial.c  -o ${OBJECTDIR}/clearSerial.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/clearSerial.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/clearSerial.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/getch.o: getch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/getch.o.d 
	@${RM} ${OBJECTDIR}/getch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  getch.c  -o ${OBJECTDIR}/getch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/getch.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/getch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTgetTouch.o: TFTgetTouch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTgetTouch.o.d 
	@${RM} ${OBJECTDIR}/TFTgetTouch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTgetTouch.c  -o ${OBJECTDIR}/TFTgetTouch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTgetTouch.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTgetTouch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/serialinitialize.o: serialinitialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serialinitialize.o.d 
	@${RM} ${OBJECTDIR}/serialinitialize.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  serialinitialize.c  -o ${OBJECTDIR}/serialinitialize.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/serialinitialize.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/serialinitialize.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/putch.o: putch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putch.o.d 
	@${RM} ${OBJECTDIR}/putch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  putch.c  -o ${OBJECTDIR}/putch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putch.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/putch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTprintDecRight.o: TFTprintDecRight.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTprintDecRight.o.d 
	@${RM} ${OBJECTDIR}/TFTprintDecRight.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTprintDecRight.c  -o ${OBJECTDIR}/TFTprintDecRight.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTprintDecRight.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTprintDecRight.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTputsTT.o: TFTputsTT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTputsTT.o.d 
	@${RM} ${OBJECTDIR}/TFTputsTT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTputsTT.c  -o ${OBJECTDIR}/TFTputsTT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTputsTT.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTputsTT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/tputs.o: tputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/tputs.o.d 
	@${RM} ${OBJECTDIR}/tputs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  tputs.c  -o ${OBJECTDIR}/tputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/tputs.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/tputs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/delay.o: delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/delay.o.d 
	@${RM} ${OBJECTDIR}/delay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  delay.c  -o ${OBJECTDIR}/delay.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/delay.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/delay.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTsend2.o: TFTsend2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTsend2.o.d 
	@${RM} ${OBJECTDIR}/TFTsend2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTsend2.c  -o ${OBJECTDIR}/TFTsend2.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTsend2.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTsend2.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTsend4.o: TFTsend4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTsend4.o.d 
	@${RM} ${OBJECTDIR}/TFTsend4.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTsend4.c  -o ${OBJECTDIR}/TFTsend4.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTsend4.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTsend4.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/idleSync.o: idleSync.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/idleSync.o.d 
	@${RM} ${OBJECTDIR}/idleSync.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  idleSync.c  -o ${OBJECTDIR}/idleSync.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/idleSync.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/idleSync.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/sendWord.o: sendWord.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sendWord.o.d 
	@${RM} ${OBJECTDIR}/sendWord.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  sendWord.c  -o ${OBJECTDIR}/sendWord.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/sendWord.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/sendWord.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTGcolors.o: TFTGcolors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGcolors.o.d 
	@${RM} ${OBJECTDIR}/TFTGcolors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTGcolors.c  -o ${OBJECTDIR}/TFTGcolors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGcolors.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTGcolors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/putString.o: putString.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/putString.o.d 
	@${RM} ${OBJECTDIR}/putString.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  putString.c  -o ${OBJECTDIR}/putString.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/putString.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/putString.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTGaddPoint.o: TFTGaddPoint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o.d 
	@${RM} ${OBJECTDIR}/TFTGaddPoint.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTGaddPoint.c  -o ${OBJECTDIR}/TFTGaddPoint.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTGaddPoint.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTGaddPoint.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/graphingStrings.o: graphingStrings.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/graphingStrings.o.d 
	@${RM} ${OBJECTDIR}/graphingStrings.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  graphingStrings.c  -o ${OBJECTDIR}/graphingStrings.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/graphingStrings.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/graphingStrings.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/clearSerial.o: clearSerial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/clearSerial.o.d 
	@${RM} ${OBJECTDIR}/clearSerial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  clearSerial.c  -o ${OBJECTDIR}/clearSerial.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/clearSerial.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/clearSerial.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/getch.o: getch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/getch.o.d 
	@${RM} ${OBJECTDIR}/getch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  getch.c  -o ${OBJECTDIR}/getch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/getch.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/getch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/TFTgetTouch.o: TFTgetTouch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/TFTgetTouch.o.d 
	@${RM} ${OBJECTDIR}/TFTgetTouch.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  TFTgetTouch.c  -o ${OBJECTDIR}/TFTgetTouch.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/TFTgetTouch.o.d"        -g -omf=elf -O0 -msmart-io=1 -Wall -msfr-warn=off
	@${FIXDEPS} "${OBJECTDIR}/TFTgetTouch.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/graphicsHostLibrary.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
