/*! \file  serialinitialize.c
 *
 *  \brief Initialize the serial port
 *
 *
 *  \author jjmcd
 *  \date June 19, 2015, 8:35 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/* Baud rate generator calculations             */
#include "processorFrequency.h"
/*! Desired baud rate                           */
#define BAUDRATE         38400
/*! Value for the baud rate generator           */
#define BRGVAL          ((FCY/BAUDRATE)/16)-1

/*! Initialize the UART */
/*! serialInitialize() initializes UART1 to the selected baud rate and the
 *  desired set of pins.
 *
 * \param bAlternate int 0 to use default UART pins, 1 to use alternate pins
 * \returns none
 */
void serialInitialize( int bAlternate )
{
  /*****************************************
   *  bAlternate ignored on PIC24FV32KA301
   *****************************************/

  _ANSB2 = 0;               /* Make the UART Rx pin digital */

  U1BRG = BRGVAL;           /* Set up baud rate generator   */

  /* Don't know why but individual bit setting fails here   */
  U1MODE = 0x8000;          /* Enable the UART              */
  U1STA = 0x0440;           /* Reset status, enable Tx      */

  _U1RXIF = 0;              /* Clear UART RX Interrupt Flag */
}

