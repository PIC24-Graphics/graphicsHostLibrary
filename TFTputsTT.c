/*! \file  TFTputsTT.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 28, 2015, 9:29 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"


/*! TFTputsTT - */

/*!
 *
 */
void TFTputsTT( char *p )
{
  putch(TFTPUTSTT);
  tputs(p);
  putch(0);
}
