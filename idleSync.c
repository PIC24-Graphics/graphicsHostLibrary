/*! \file  idleSync.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 12:36 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLibrary.h"

/*! Send a bunch of nulls to guarantee sync with terminal */
/*!
 *
 */
void idleSync(void)
{
  int i;
  for ( i=0; i<20; i++ )
    {
      putch(0);
      delay(10);
    }
}
