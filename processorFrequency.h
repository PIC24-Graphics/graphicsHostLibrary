/*! \file  processorFrequency.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 29, 2015, 2:53 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef PROCESSORFREQUENCY_H
#define	PROCESSORFREQUENCY_H

#ifdef	__cplusplus
extern "C"
{
#endif

//! On-board Crystal frequency
#define XTFREQ          8000000
//! On-chip PLL setting
#define PLLMODE         2
//! Instruction Cycle Frequency
#define FCY             XTFREQ*PLLMODE/2



#ifdef	__cplusplus
}
#endif

#endif	/* PROCESSORFREQUENCY_H */

