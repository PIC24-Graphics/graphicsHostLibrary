/*! \file  sendWord.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 12:37 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"

/*! sendWord - */

/*!
 *
 */
void sendWord(unsigned int word)
{
  putch(word&0xff);
  putch((word&0xff00)>>8);
}

