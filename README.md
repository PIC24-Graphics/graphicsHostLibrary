## Graphics host library for PIC24FV

This project provides an easy way for the 16-bit PIC programmer to use
the serial graphics terminal.  The project as written uses a
PIC24FV32KA301, but should be usable on any 16 or 32 bit PIC with few,
if any, modifications.

The library implements the wire protocol described in
https://gitlab.com/PIC32MX/Wire_Protocol, but the doxygen file,
doxygen/latex/refman.pdf is probably a better reference for most
programmers.

One modification highly likely is to the file processorFrequency.h.
This file is set up to use the internal oscillator of the PIC24 with
the PLL resulting in a 16 MIPS instruction rate.  Different oscillator
choices will require that this file be adjusted to compensate for the
different clock rate.

For some processors it may be necessary to adjust serialinitialize.c
and possibly putch.c, but the author has used these files with a
variety of 16-bit PICs and has yet to encounter the need to make a
change.

The touch side of this software is not yet complete, but when used, it
may be that getch.c will also require adjustment.

Related:

*  https://gitlab.com/PIC32MX/graphicsTerminal - The client code that this project works with
*  https://gitlab.com/PIC32MX/tft - A PIC32 graphics library that is used by the serial graphics terminal
*  https://gitlab.com/PIC32MX/Wire_Protocol - Documentation of the serial interface of the graphics terminal

