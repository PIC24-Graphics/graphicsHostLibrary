/*! \file  TFTGaddPoint.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 1, 2015, 1:13 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graphicsHostLocal.h"

/*! TFTGaddPoint - */

/*!
 *
 */
void TFTGaddPoint(int y)
{
  putch(TFTGADDPT);
  sendWord(y);
}
