/*! \file  graphicsHostLibrary.h
 *
 *  \brief Graphics Host Library - Host Interface
 *
 * \page p1 Introduction
 *
 * This file contains the specifications for the public interface for
 * a library to be used by applications utilizing the serial graphics
 * terminal.
 *
 * \section use Using this library
 *
 * This library is distributed as source so that it may be compiled for
 * whatever PIC the application requires.  The assumption is a 16-bit PIC,
 * although a 32-bit PIC should probably work as well.  The library has been
 * written for the XC16 compiler.
 *
 * The library should be compiled after the appropriate customizations
 * (see below), and assembled into a library using the \c xc16-ar application
 * which is invoked automatically by the MPLAB-X library project if MPLAB-X
 * is used.
 *
 * The user should then include that library in his project, and functions
 * requiring access to the graphics terminal should include this file an
 * \c colors.h.  This file, in turn, includes \c graphicsHostLocal.h which should
 * be in the same directory as this file.  \c graphicsHostLocal.h defines
 * functions local to the library which should not be directly called
 * by the application as their interface is not protected.
 *
 * \section mod Modification for other PICs
 *
 * Most of this library should be applicable to any 16-bit PIC.  However,
 * the file \c processorFrequency.h will need to be adjusted for the specific
 * processor speed selected, and \c serialInitialize.c will likely also need
 * to be adjusted.  \c putch.c \em may need modification, as well, especially
 * if the user desires to use the second UART.  All the other functions and
 * macros are independent of the target processor and should require no
 * modification.
 *
 * Note that \c processorFrequency.h must result in the appropriate \c FCY
 * but the calculation might be different for different processors, so it
 * is not sufficient to merely modify the oscillator frequency.
 *
 * For example, \c processorFrequency.h for a PIC24FV32MA301 using the
 * internal RC oscillator looks like:
 * \code{.c}
 * // On-board Crystal frequency
 * #define XTFREQ          8000000
 * // On-chip PLL setting
 * #define PLLMODE         2
 * // Instruction Cycle Frequency
 * #define FCY             XTFREQ*PLLMODE/2
 * \endcode
 * while for a dsPIC30F4011 using a crystal and the PLL it might look like:
 * \code{.c}
 * // On-board Crystal frequency
 * #define XTFREQ          7372800
 * // On-chip PLL setting
 * #define PLLMODE         16
 * // Instruction Cycle Frequency
 * #define FCY             XTFREQ*PLLMODE/4
 * \endcode
 * In some cases, manipulation of \c OSCCON and/or \c CLKDIV by the
 * application might further affect the calculation of \c FCY.  \c FCY
 * is used in both delay() and in the calculation for the baud rate
 * generator in serialInitialize().
 *
 * \section calling Calling the functions
 *
 * The application will call serialInitialize() first, then TFTinit(),
 * generally followed by some color setting functions before performing
 * actual drawing.  For example:
 *
 * \code{.c}
 * #include "graphicsHostLibrary.h"
 * #include "colors.h"
 *
 *    ...
 *
 *    serialInitialize(0);     // Initialize the serial port
 *    TFTinit(LANDSCAPE);      // Initialize the TFT to landscape mode
 *    TFTsetColorX(WHITE);     // Set the drawing color to white
 *    TFTsetBackColorX(RED);   // Set the background color to res
 *    TFTclear();              // Clear the screen
 *    TFTrect(50,50,270,190);  // Draw a rectangle 50 pixels from eash edge
 * \endcode
 * 
 * will draw a white rectangle on a red screen.
 *
 * The application may choose to do it's own serial port initialization, in
 * which case serialInitialize() is not needed.  The port must be set to
 * 38k4 unless the TFT speed jumper is set, in which case the baud rate
 * is 9k6.
 *
 * \section gra Graph Functions
 *
 * The graphing functions represent a special case in that much state
 * information is kept by the terminal.  Graphs are created by calling
 * TFTGinit(), followed optionally by a series of color and labeling
 * functions then TFTGexpose() and finally a number of calls to
 * TFTGaddPoint().  For example:
 *
 * \code{.c}
 * #include "graphicsHostLibrary.h"
 * #include "colors.h"
 *
 *    ...
 *
 *   serialInitialize(0);         // Initialize the serial port
 *   TFTinit(LANDSCAPE);          // Initialize the TFT to landscape mode
 *     ...
 *   TFTGinit();
 *   TFTGrange(0,10,0,100);       // X from 0..10, Y 0..100
 *   TFTGXlabel("Weeks");         // Label the X axis
 *   TFTGtitle("Price of corn");  // Graph title
 *   TFTGexpose();                // Show the graph decorations
 *   // Now draw the data
 *   for ( i=0; i<100; i++ )
 *       TFTGaddPoint(price[i]);
 * \endcode
 *
 * Each call to TFTGaddPoint moves one pixel to the right within the graph
 * plotting area.  When the right edge is reached, the plot wraps and the
 * previous trace is erased as the new trace is drawn.  This erasing behavior
 * may be modified with the functions TFTGnoErase() and TFTGnoCursor().
 * 
 */
/*  \author jjmcd
 *  \date June 19, 2015, 9:45 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef GRAPHICSHOSTLIBRARY_H
#define	GRAPHICSHOSTLIBRARY_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include "graphicsHostLocal.h"


/*! Initialize - set mode to portrait */
#define TFTPORTRAIT         0x00
/*! Initialize - set mode to landscape */
#define TFTLANDSCAPE        0x01

/*! Set Font - Small font */
#define FONTSMALL           0x01
/*! Set Font - Big font */
#define FONTBIG             0x02
/*! Set Font - Seven segment number-only font */
#define FONTSEVENSEGNUM     0x03
/*! Set Font - Deja Vu Sans */
#define FONTDJS             0x04
/*! Set Font - Deja Vu Sans Bold */
#define FONTDJSB            0x05
/*! Set Font - Smallbold */
#define FONTBOLDSMALL       0x06

/*! PrintInt - Left justified, decimal */
#define TFTPRINTINTPLAIN    0x0000
/*! PrintInt - Left justified, hexadecimal*/
#define TFTPRINTINTHEX      0x0001
/*! PrintInt - Right justify */
#define TFTPRINTINTRIGHT    0x0002
/*! PrintInt - Zero fill */
#define TFTPRINTINTZERO     0x0004

  
/*! TFTinit() - Initialize the TFT */
/*! Initializes the TFT and the TFT supporting software
 *
 * \param fmt unsigned char - format PORTRAIT or LANDSCAPE
 */
#define TFTinit(fmt) {  putch(TFTINIT);  putch(fmt); idleSync(); }

/*! TFTsetColorX() - Set the drawing color (16-bit) */
/*! Set the current drawing color.
 *
 * \param color unsigned int - The 16-bit color (typically from colors.h)
 */
#define TFTsetColorX(color) {putch(TFTSETCOLORX);sendWord(color);}

/*! TFTsetBackColorX() - Set the background color (16-bit) */
/*! Set the current background color.
 *
 * \param color unsigned int - The 16-bit color (typically from colors.h)
 */
#define TFTsetBackColorX(color) {putch(TFTSETBACKCOLORX);sendWord(color);}

/*! TFTsetColor() - set the drawing color */
/*! Set the drawing color by providing red, green and blue components. Note
 *  that the display is only 16 bits deep so the least significant portions
 *  of the specified colors are lost.
 *
 * \param r unsigned byte - Red portion of the color
 * \param g unsigned byte - Green portion of the color
 * \param b unsigned byte - Blue portion of the color
 */
#define TFTsetColor(r,g,b) { putch(TFTSETCOLOR); putch(r); putch(g); putch(b);}

/*! TFTsetBackColor() - set the background color */
/*! Set the background color by providing red, green and blue components. Note
 *  that the display is only 16 bits deep so the least significant portions
 *  of the specified colors are lost.
 *
 * \param r unsigned byte - Red portion of the color
 * \param g unsigned byte - Green portion of the color
 * \param b unsigned byte - Blue portion of the color
 */
#define TFTsetBackColor(r,g,b) { putch(TFTSETBACKCOLOR); putch(r); putch(g); putch(b);}

/*! TFTclear() - Clear the screen */
/*! Clear the screen by filling it with the current background color
 *
 * \param none
 */
#define TFTclear() { putch(TFTCLEAR); }

/*! TFTpixel() - Draw a pixel */
/*! Draw a pixel on the screen with the current drawing color
 *
 * \param x int - Horizontal position of the pixel
 * \param y int - Vertical position of the pixel
 */
#define TFTpixel(x,y) { TFTsend2(TFTPIXEL,x,y); }

/*! TFTline() - Draw a line */
/*! Draw a line in the currently selected drawing color
 *
 * \param x1 int - Horizontal position of the beginning of the line
 * \param y1 int - Vertical position of the beginning of the line
 * \param x2 int - Horizontal position of the end of the line
 * \param y2 int - Vertical position of the end of the line
 */
#define TFTline(x1,y1,x2,y2) { TFTsend4(TFTLINE,x1,y1,x2,y2); }

/*! TFTrect() - Draw a rectangle */
/*! Draws an open rectangle in the current foreground color.
 *
 * \param x1 unsigned int - left edge of rectangle
 * \param y1 unsigned int - top edge of rectangle
 * \param x2 unsigned int - right edge of rectangle
 * \param y2 unsigned int - bottom edge of rectangle
 */
#define TFTrect(x1,y1,x2,y2) { TFTsend4(TFTRECT,x1,y1,x2,y2); }

/*! TFTfillRect() - Draw a filled rectangle */
/*! Draws a rectangle filled with the current foreground color.
 *
 * \param x1 unsigned int - left edge of rectangle
 * \param y1 unsigned int - top edge of rectangle
 * \param x2 unsigned int - right edge of rectangle
 * \param y2 unsigned int - bottom edge of rectangle
 */
#define TFTfillRect(x1,y1,x2,y2) { TFTsend4(TFTFILLRECT,x1,y1,x2,y2); }

/*! TFTclearRect() - Clear a rectangle with a specified color */
/*! Fill a rectangle with the specified color
 *
 * \param r unsigned char - Red component of the desired color
 * \param g unsigned char - Green component of the desired color
 * \param b unsigned char - Blue component of the desired color
 * \param x1 int - Horizontal position of the left edge of the rectangle
 * \param y1 int - Vertical position of the top edge of the rectangle
 * \param x2 int - Horizontal position of the right edge of the rectangle
 * \param y2 int - Vertical position of the bottom edge of the rectangle
 */
#define TFTclearRect(r,g,b,x1,y1,x2,y2) { putch(TFTCLEARRECT);putch(r);putch(g);putch(b);sendWord(x1);sendWord(y1);sendWord(x2);sendWord(y2);}

/*! TFTeraseRect() - Erase a rectangular area of the screen */
/*! Erases a portion of the screen by filling it with the background color
 *
 * \param x1 int - Horizontal position of the left edge of the rectangle
 * \param y1 int - Vertical position of the top edge of the rectangle
 * \param x2 int - Horizontal position of the right edge of the rectangle
 * \param y2 int - Vertical position of the bottom edge of the rectangle
 */
#define TFTeraseRect(x1,y1,x2,y2) { TFTsend4(TFTERASERECT,x1,y1,x2,y2); }

/*! TFTroundRect() - Draw a rounded rectangle */
/*! Draws an open rounded rectangle in the current foreground color.
 *
 * \param x1 unsigned int - left edge of rectangle
 * \param y1 unsigned int - top edge of rectangle
 * \param x2 unsigned int - right edge of rectangle
 * \param y2 unsigned int - bottom edge of rectangle
 */
#define TFTroundRect(x1,y1,x2,y2) { TFTsend4(TFTROUNDRECT,x1,y1,x2,y2); }

/*! TFTfilledRoundRect() - Draw a rounded rectangle */
/*! Draws a filled rounded rectangle in the current foreground color.
 *
 * \param x1 unsigned int - left edge of rectangle
 * \param y1 unsigned int - top edge of rectangle
 * \param x2 unsigned int - right edge of rectangle
 * \param y2 unsigned int - bottom edge of rectangle
 */
#define TFTfilledRoundRect(x1,y1,x2,y2) { TFTsend4(TFTFILLROUNDRECT,x1,y1,x2,y2); }

/*! TFTcircle() - Draw a circle */
/*! Draws a circle in the current foreground color
 *
 * \param x unsigned int - Horizontal position of the center of the circle
 * \param y unsigned int - Vertical position of the center of the circle
 * \param r unsigned int - Radius of the circle
 */
#define TFTcircle(x,y,r) { putch(TFTCIRCLE); sendWord(x); sendWord(y); sendWord(r); }

/*! TFTfillCircle() - Draw a filled circle */
/*! Draws a circle filled with the current foreground color
 *
 * \param x unsigned int - Horizontal position of the center of the circle
 * \param y unsigned int - Vertical position of the center of the circle
 * \param r unsigned int - Radius of the circle
 */
#define TFTfillCircle(x,y,r) { putch(TFTFILLCIRCLE); sendWord(x); sendWord(y); sendWord(r); }

/*! TFTsetFont() - Set the current font */
/*! Set the font to be used for future charater operations.  The
 *  font is one of:
 *  \li FONTSMALL
 *  \li FONTBIG
 *  \li FONTDJS
 *  \li FONTSEVENSEGNUM
 *
 * Note that FONTBOLDSMALL and FONTDJSB will also be accepted but are
 * under development.
 *
 *  \param font char - Font to be selected
 */
#define TFTsetFont(font) {putch(TFTSETFONT);putch(font);}

/*! TFTprintChar() - Print a character on the display
 *  Print a character on the display in the current drawin color and font.
 *
 * \param ch char - Character to be displayed
 * \param x int - Horizontal location of character
 * \param y int - Vertical location of character
 */
#define TFTprintChar(ch,x,y) {putch(TFTPRINTCHAR); putch(ch); sendWord(x); sendWord(y); }

/*! TFTputsTT() - Print a string, teletype style */
/*! Prints a string on the screen in the current font.  Text will wrap to
 *  the next line when reaching the right side of the screen, and will wrap
 *  to the top of the screen when the bottom is reached.  Carriage return
 *  and line feed behave as expected.  The form feed code \c 0x0c clears the
 *  screen and positions the cursor to line 1 column 1.  The ANSI escape
 *  code sequences \c H \c J and \c K behave as expected.  No other ANSI
 *  sequences are supported.
 *
 * \param p char * - Pointer to the string to be displayed
 */
void TFTputsTT( char * );

/*! TFTprintInt() - Print an integer */
/*! Prints an integer in any of several formats
 *
 * The fmt parameter may be an ORed combination of
 * \li TFTPRINTINTPLAIN - no special formatting
 * \li TFTPRINTINTHEX - print in hexadecimal
 * \li TFTPRINTINTRIGHT - print right justified
 * \li TFTPRINTINTZERO - print zero filled
 *
 * \param val int - Number to be printed
 * \param fmt int - Desired format
 * \param x   int - Horizontal location on the screen
 * \param y   int - Vertical location on the screen
 */
#define TFTprintInt(val,fmt,x,y) { TFTsend4(TFTPRINTINT,val,fmt,x,y); }

/*! TFTprintDec() - Print a number with a decimal */
/*! Prints a number and inserts a decimal point in the location indicated
 *  by the caller.
 * 
 * \param val int - Number to be printed
 * \param loc int - Position of the decimal
 * \param x   int - Horizontal location on the screen
 * \param y   int - Vertical location on the screen
 */
#define TFTprintDec(val,loc,x,y) { TFTsend4(TFTPRINTDEC,val,loc,x,y); }

/*! TFTprintDecRight - Print a right-justified integer with inserted decimal */
/*! Display an integer on the screen with an inserted decimal point
 *  right-justified
 *
 * \param val int - value to be printed
 * \param dec int - number of places from the right to place the decimal
 * \param x int - horizontal position of the right end of the number on the screen
 * \param y int - vertical position of the number on the screen
 */
void TFTprintDecRight( int, int, int, int );

/*! TFTtransitionOpen() - Clear the screen starting at the middle */
/*! Draws rectangles from the center of the screen to the outside
 *  until the entire screen is filled.
 * 
 * \param c unsigned int - New screen color
 */
#define TFTtransitionOpen(c) { putch(TFTTRANSITIONOPEN); sendWord(c); delay(1500); }

/*! TFTtransitionClose() - Clear the screen starting at the outside */
/*! Draws rectangles from the outside of the screen decreasing in size
 *  until the entire screen is filled.
 *
 * \param c unsigned int - New screen color
 */
#define TFTtransitionClose(c) { putch(TFTTRANSITIONCLOSE); sendWord(c); delay(500); }

/*! TFTsoftwareID() - Display the terminal software version */
/*! Displays a screen showing the terminal program compile date and
 *  time and commit hash.
 *
 * \param none
 */
#define TFTsoftwareID() { putch('#'); }

/*! TFTfatalError() - Display a fatal error */
/*! Disables the serial interrupt on the TFT board and turns off
 *  the UART.  Displays an error message and flashes the LED
 *  quickly.
 *
 * \param none
 */
#define TFTfatalError() { putch(TFTFATALERROR); while(1); }

/*! TFTGrange() - Set the scales for a graph */
/*! Sets the scales for a graph.  Must be called before TFTGexpose().
 *  TFTGinit() will erase these values so this function should generally
 *  be called after TFTGinit();
 *
 *  \param x1 int - Minimum value for the X-axis
 *  \param x2 int - Maximum value for the X-axis
 *  \param y1 int - Minimum value for the Y-axis
 *  \param y2 int - Maximum value for the Y-axis
 */
#define TFTGrange(x1,x2,y1,y2) { TFTsend4(TFTGRANGE,x1,x2,y1,y2); }

/*! TFTGinit() - Initialize a graph */
/*! This function initializes the simple graphing system.  Titles and
 *  annotation are cleared, scales are erased, and a default set of
 * colors is set as follows:
 * \li Screen background \c GRAY13
 * \li Plot area \c BLACK
 * \li Axis annotation \c DARKCYAN
 * \li X label \c LIGHTSEAGREEN
 * \li Y label \c LIGHTSEAGREEN
 * \li Title \c GOLD
 * \li Plotted data \c PERU
 *
 * \param nont
 */
#define TFTGinit() { putch(TFTGINIT); }

/*! TFTGexpose() - Show the graph frame */
/*! Draw colors the screen in the screen background color.  Fills the
 *  plot area with the plot color.  Draws the axis annotation if scales
 *  have been selected, annotates the scales if annotation has been
 *  provided, and draws the title if a title has been provided.  Colors
 *  are those set by TFTGcolors() unless that function has not been called
 *  since the last TFTGinit() in which case the default colors are used.
 *
 * \param none
 */
#define TFTGexpose() { putch(TFTGEXPOSE); }

/*! TFTGcolors() - Set the colors for a graph */
/*! Determines the set of colors to be used for a graph.  Must be called
 *  after TFTGinit() and before TFTGexpose().  TFTGinit() will establish
 *  a useful set of colors.  If different colors are desired, this
 *  function will override the TFTGinit() colors.
 *
 * \param ctit unsigned int - Color to be  used for the graph title
 * \param cann unsigned int -  Color to be used for graph annotation
 * \param cxlab unsigned int - Color to be used for the X-axis label
 * \param cylab unsigned int - Color to be used for the Y-axis label
 * \param cscreen unsigned int - Color to be used for the background (graph outside)
 * \param cfield unsigned int - Color to be used for the graph plotting area
 * \param cdata unsigned int - Color to be used for the graph data
 */
void TFTGcolors( unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int );

/*! TFTGaddPoint() - Add a point to a graph */
/*! Adds a point to an existing graph.  Each point is plotted one pixel to
 * the right of the previous pixel, and wraps when the right edge is reached.
 * Values outside the range are limited to the range.
 *
 *  \param y int - Y value of the point to be plotted
 */
void TFTGaddPoint( int );

/*! TFTGtitle() - Add a title to a graph */
/*! Provides a title for the graph.  The title is drawn at the top centered
 *  on the graph area unless the title is too long, in which case it is
 *  centered on the entire screen.
 *
 *  \param p char * - Pointer to the string containing the title
 */
#define TFTGtitle(p) { graphingStrings(TFTGTITLE,p); }

/*! TFTGXlabel() - Label the X axis of the graph */
/*! Provides a label for the horizontal axis of the graph
 *
 *  \param p char * - Pointer to the string containing the label
 */
#define TFTGXlabel(p) { graphingStrings(TFTGXLABEL,p); }

/*! TFTGYlabel() - Label the Y axis of the graph */
/*! Provides a label for the vertical axis of the graph.
 *
 *  \param p char * - Pointer to the string containing the label
 */
#define TFTGYlabel(p) { graphingStrings(TFTGYLABEL,p); }

/*! TFTgetTouch() - Query the TFT for a touch response */
/*! TFTgetTouch queries the TFT for a touch event.  If the screen is
 *  being touched, the coordinates of the touch are stored in
 *  x and y.  If not, x and y contain 0xffff.  The function returns
 *  true if there was a touch event, false if not
 *
 * \param x unsigned int * - Pointer to horizontal coordinate of the touch event
 * \param y unsigned int * - Pointer to vertical coordinate of the touch event
 * \return True if touched, otherwise false
 */
int TFTgetTouch(unsigned int *, unsigned int * );


/*! delay() - Delay a specified number of milliseconds */
/*! Provides a delay for a specified number of milliseconds.  This delay
 *  is dependent on the processor frequency provided in \c processorFrequency.h
 *
 * \param d unsigned long - Number of milliseconds to delay
 */
void delay(unsigned long );


#ifdef	__cplusplus
}
#endif

#endif	/* GRAPHICSHOSTLIBRARY_H */

