/*! \file  getch.c
 *
 *  \brief Get a character from the serial port
 *
 *
 *  \author jjmcd
 *  \date July 14, 2015, 7:49 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! getch() -  Get a serial character */

/*! Waits until a character is available on UART1 and returns the character
 *
 * \param none
 * \return unsigned char from serial port
 */
unsigned char getch( void )
{
  /*  while ( !_U1RXIF ) ; */
  _U1RXIF = 0;          /* Clear the interrupt flag             */
  while (!_URXDA);      /* Wait until a character available     */
  return U1RXREG;       /* Return the character                 */
}
